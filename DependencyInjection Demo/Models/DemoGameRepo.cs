﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyInjection_Demo.Models
{
    public class DemoGameRepo : IGameRepository
    {
        private List<Games> _games;
        public DemoGameRepo()
        {
            _games = new List<Games>()
            {
                new Games(){Id=1, Name="Valorant"},
                new Games(){Id=2, Name="CSGO"},
            };
        }
        public Games Add(Games game)
        {

            _games.Add(game);
            return game;
        }

        public IEnumerable<Games> GetAllGames()
        {
            return _games;
        }
    }
}
