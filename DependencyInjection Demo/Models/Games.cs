﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyInjection_Demo.Models
{
    public class Games
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
