﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyInjection_Demo.Models
{
    public interface IGameRepository
    {
        IEnumerable<Games> GetAllGames();
        Games Add(Games game);
    }
}
