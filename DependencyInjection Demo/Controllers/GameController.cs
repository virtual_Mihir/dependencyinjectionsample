﻿using DependencyInjection_Demo.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyInjection_Demo.Controllers
{
    public class GameController : Controller
    {
        private IGameRepository _gameRepository;
        public GameController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }
        [HttpGet]
        public ViewResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Games game)
        {
            if (ModelState.IsValid)
            {
                Games game1 = _gameRepository.Add(game);
            }
            return View();
        }
    }
}
